# Hulder
A attempt at modelling a forest creature/spirit in Blender.

A hulder (or huldra) is a seductive forest creature found in Scandinavian folklore. Her name derives from a root meaning "covered" or "secret".
 In Norwegian folklore, she is known as huldra ("the [archetypal] hulder", though folklore presupposes that there is an entire Hulder race and not just a single individual). She is known as the skogsrå "forest spirit" or Tallemaja "pine tree Mary" in Swedish folklore, and ulda in Sámi folklore. Her name suggests that she is originally the same being as the völva divine figure Huld and the German Holda.

The word hulder is only used of a female; a "male hulder" is called a huldrekall and also appears in Norwegian folklore. This being is closely related to other underground dwellers, usually called tusser (sg., tusse).

Though described as beautiful, the huldra is noted for having a distinctive inhuman feature—an animal's tail (usually a cow's in Norway or a fox's in Sweden). It falls off when married.

The hulders were held to be kind to charcoal burners, watching their charcoal kilns while they rested. Knowing that she would wake them if there were any problems, they were able to sleep, and in exchange they left provisions for her in a special place. A tale from Närke illustrates further how kind a hulder could be, especially if treated with respect (Hellström 1985:15). This also was supposedly the case for the females who mided the summer mountain farms.

She also in some districts have a dark erotic trait,- If you as a man is capable of satisfying her needs you will be rewarded with gold... But if you don't,- whoe upon you! 

To catch/tame her you have to trow a piece of steel over her head. She is quite beautiful, and a herd of grey cows and riches follows as dowry.
The tail falls off during the marriage cerenomy.
But you better be nice,- for even if married she is immensely strong.

There are a story of a man who didn't heed her calls for dinner,- so she had enough and came into the smithy,- took a red glowing horse shoe out from the forge and straightened it out using her bare hands and said "I could easily hurt you if I want".







It has some issues,- the rigging needs to have its weight painting checked.
I have tried to get the hair to work,but that also needs to be rigged.
The clothing should also been looked at, its partually rigged but should have IK chains added and merged with the existing rig.

image: 
![Image](https://gitlab.com/supermag/Hulder/-/blob/main/hulder.png)






Image from public domain, Theodor Kittelsen
![Image from public domain, Theodor Kittelsen](https://gitlab.com/supermag/Hulder/-/blob/main/NG.K_H.B.06921.jpg)


